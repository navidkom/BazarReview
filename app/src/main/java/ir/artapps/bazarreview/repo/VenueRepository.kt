package ir.artapps.bazarreview.repo

import androidx.lifecycle.LiveData
import ir.artapps.bazarreview.entities.GetVenueResponseModel
import ir.artapps.bazarreview.entities.Venue

/**
 *   Created by Navid Komijani
 *   on 23,February,2020
 */

interface VenueRepository {
    fun getVenuesLiveData(): LiveData<List<Venue>>
    suspend fun getNearVenues(lat: Double, lang: Double, firstPage: Boolean): Int
    suspend fun getVenue(venueId: String): GetVenueResponseModel?
}