package ir.artapps.bazarreview

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

/**
 *   Created by Navid Komijani
 *   on 29,February,2020
 */
class App: Application() {
    override fun onCreate() {
        super.onCreate()

        // Start Koin
        startKoin{
            androidLogger()
            androidContext(this@App)
            androidFileProperties()
            modules(Module().appModule )
        }
    }
}