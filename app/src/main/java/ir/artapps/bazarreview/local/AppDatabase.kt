package ir.artapps.bazarreview.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

/**
 * Created by navid
 */

@TypeConverters(DatabseConverter::class)
@Database(entities = [VenueRoomEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract val venuesDAO: VenuesDAO
}