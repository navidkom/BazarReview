package ir.artapps.bazarreview.local

import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ir.artapps.bazarreview.entities.Category
import ir.artapps.bazarreview.entities.Contact
import ir.artapps.bazarreview.entities.Location
import ir.artapps.bazarreview.entities.Venue
import ir.artapps.bazarreview.local.VenueRoomEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME, indices = [Index(value = ["id"], unique = true)])
data class VenueRoomEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_")
    @Transient
    var id_: Long = 0,

    @SerializedName("id")
    @Expose
    val id: String? = null,

    @SerializedName("name")
    @Expose
    val name: String? = null,

    @SerializedName("location")
    @Expose
    val location: Location? = null,

    @SerializedName("latitude")
    @Expose
    val latitude: Double = 0.0,

    @SerializedName("longitude")
    @Expose
    val longitude: Double = 0.0,

    @SerializedName("icon")
    @Expose
    val icon: String? = null,

    @SerializedName("iconPrefix")
    @Expose
    val iconPrefix: String? = null,

    @SerializedName("canonicalUrl")
    @Expose
    val canonicalUrl: String? = null,

    @SerializedName("contact")
    @Expose
    val contact: Contact? = null,

    @SerializedName("categories")
    @Expose
    val categories: List<Category>? = null,

    @SerializedName("verified")
    @Expose
    val verified: Boolean? = null,

    @SerializedName("url")
    @Expose
    val url: String? = null,

    @SerializedName("dislike")
    @Expose
    val dislike: Boolean? = null,

    @SerializedName("ok")
    @Expose
    val ok: Boolean? = null,

    @SerializedName("rating")
    @Expose
    val rating: Double? = null,

    @SerializedName("ratingColor")
    @Expose
    val ratingColor: String? = null,

    @SerializedName("ratingSignals")
    @Expose
    val ratingSignals: Int? = null,

    @SerializedName("createdAt")
    @Expose
    val createdAt: Int? = null,

    @SerializedName("shortUrl")
    @Expose
    val shortUrl: String? = null,

    @SerializedName("timeZone")
    @Expose
    val timeZone: String? = null
) {
    companion object {
        @Ignore
        @Transient
        const val TABLE_NAME = "venue"
    }

    constructor (v: Venue) : this(
        0,
        v.id,
        v.name,
        v.location,
        0.0,
        0.0,
        null,
        null,
        v.canonicalUrl,
        v.contact,
        v.categories,
        v.verified,
        v.url,
        v.dislike,
        v.ok,
        v.rating,
        v.ratingColor,
        v.ratingSignals,
        v.createdAt,
        v.shortUrl,
        v.timeZone
    )


    fun toVenue(): Venue {
        return Venue(
            id,
            name,
            location,
            canonicalUrl,
            contact,
            categories,
            verified,
            url,
            dislike,
            ok,
            rating,
            ratingColor,
            ratingSignals,
            createdAt,
            shortUrl,
            timeZone
        )
    }
}