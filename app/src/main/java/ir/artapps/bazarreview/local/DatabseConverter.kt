package ir.artapps.bazarreview.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.artapps.bazarreview.entities.Category
import ir.artapps.bazarreview.entities.Contact
import ir.artapps.bazarreview.entities.Location

class DatabseConverter {
    val gson = Gson()
    @TypeConverter
    fun locationToString(location: Location?): String {
        return gson.toJson(location)
    }

    @TypeConverter
    fun stringToLocation(data: String?): Location {
        val type = object : TypeToken<Location?>() {}.type
        return gson.fromJson(data, type)
    }

    @TypeConverter
    fun contactToString(contact: Contact?): String {
        return gson.toJson(contact)
    }

    @TypeConverter
    fun stringToContact(data: String?): Contact {
        val type = object : TypeToken<Contact?>() {}.type
        return gson.fromJson(data, type)
    }

    @TypeConverter
    fun categoriesToString(categories: List<Category?>?): String {
        return gson.toJson(categories)
    }

    @TypeConverter
    fun stringToCategories(data: String?): List<Category> {
        val type =
            object : TypeToken<List<Category?>?>() {}.type
        return gson.fromJson<List<Category>>(data, type)
    }
}