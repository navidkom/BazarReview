package ir.artapps.bazarreview.local

import androidx.lifecycle.LiveData
import androidx.room.*

/**
 * Created by navid
 */
@Dao
interface VenuesDAO {
    @Query("SELECT * FROM venue WHERE latitude between :latFrom AND :latTo AND longitude between :longFrom AND :longTo")
    fun getVenueByLatlong(
        latFrom: Double,
        latTo: Double,
        longFrom: Double,
        longTo: Double
    ): List<VenueRoomEntity?>?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertVenues(venues: List<VenueRoomEntity>)

    @Query("SELECT * FROM venue order by id_ asc")
    fun loadAll(): LiveData<List<VenueRoomEntity>>

    @Query("SELECT COUNT(id) FROM venue")
    suspend fun getCount(): Int

    @Query("DELETE FROM venue")
    suspend fun invalidate()
}