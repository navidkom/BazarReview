package ir.artapps.bazarreview

import androidx.room.Room
import ir.artapps.bazarreview.local.AppDatabase
import ir.artapps.bazarreview.remote.VenueRemoteDataSource
import ir.artapps.bazarreview.remote.VenueRemoteDataSourceImpl
import ir.artapps.bazarreview.repo.VenueRepository
import ir.artapps.bazarreview.repo.VenueRepositoryImpl
import ir.artapps.bazarreview.ui.detail.VenueDetailViewModel
import ir.artapps.bazarreview.ui.main.MainViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


/**
 *   Created by Navid Komijani
 *   on 29,February,2020
 */

class Module {
    val appModule = module {
        single {
            Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "database.db")
                .build()
        }
        single { get<AppDatabase>().venuesDAO }
        single<VenueRemoteDataSource> { VenueRemoteDataSourceImpl() }
        single<VenueRepository> { VenueRepositoryImpl(get(), get()) }

        viewModel { MainViewModel(get()) }
        viewModel { VenueDetailViewModel(get()) }
    }
}