package ir.artapps.bazarreview.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by navid on 22,December,2018
 */
data class Meta(
    @SerializedName("code")
    @Expose
    var code: Int = 0,

    @SerializedName("requestId")
    @Expose
    var requestId: String? = null
)