package ir.artapps.bazarreview.entities

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Icon(
    @SerializedName("prefix")
    @Expose
    var prefix: String? = null,
    @SerializedName("suffix")
    @Expose
    var suffix: String? = null
) : Parcelable {
    private val SIZE = 64
    val url: String?
        get() = if (prefix != null && suffix != null) {
            prefix + SIZE + suffix
        } else null
}