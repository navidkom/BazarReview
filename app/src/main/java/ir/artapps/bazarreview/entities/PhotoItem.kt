package ir.artapps.bazarreview.entities

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by navid on 27,December,2018
 */

@Parcelize
data class PhotoItem(
    @SerializedName("id")
    @Expose
    var id: String? = null,

    @SerializedName("createdAt")
    @Expose
    var createdAt: Int? = null,

    @SerializedName("source")
    @Expose
    var source: Source? = null,

    @SerializedName("prefix")
    @Expose
    var prefix: String? = null,

    @SerializedName("suffix")
    @Expose
    var suffix: String? = null,

    @SerializedName("width")
    @Expose
    var width: Int? = null,

    @SerializedName("height")
    @Expose
    var height: Int? = null,

    @SerializedName("visibility")
    @Expose
    var visibility: String? = null
) : Parcelable {
    val imageUrl: String
        get() = String.format("%s%sx%s%s", prefix, width, height, suffix)
}
