package ir.artapps.bazarreview.entities

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Venue(
    @SerializedName("id")
    @Expose
    var id: String? = null,

    @SerializedName("name")
    @Expose
    var name: String? = null,

    @SerializedName("location")
    @Expose
    var location: Location? = null,

    @SerializedName("canonicalUrl")
    @Expose
    var canonicalUrl: String? = null,

    @SerializedName("contact")
    @Expose
    var contact: Contact? = null,

    @SerializedName("categories")
    @Expose
    var categories: List<Category>? =
        null,

    @SerializedName("verified")
    @Expose
    var verified: Boolean? = null,

    @SerializedName("url")
    @Expose
    var url: String? = null,

    @SerializedName("dislike")
    @Expose
    var dislike: Boolean? = null,

    @SerializedName("ok")
    @Expose
    var ok: Boolean? = null,

    @SerializedName("rating")
    @Expose
    var rating: Double? = null,

    @SerializedName("ratingColor")
    @Expose
    var ratingColor: String? = null,

    @SerializedName("ratingSignals")
    @Expose
    var ratingSignals: Int? = null,

    @SerializedName("createdAt")
    @Expose
    var createdAt: Int? = null,

    @SerializedName("shortUrl")
    @Expose
    var shortUrl: String? = null,

    @SerializedName("timeZone")
    @Expose
    var timeZone: String? = null,

    @SerializedName("bestPhoto")
    @Expose
    var bestPhoto: PhotoItem? = null,

    var distance: Double = 0.0
) : Parcelable {
    override fun toString(): String {
        return super.toString()
    }
}