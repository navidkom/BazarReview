package ir.artapps.bazarreview.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by navid on 22,December,2018
 */
data class VenuesListResponse (
    @SerializedName("venues")
    @Expose
    var venues: List<Venue>? = null,

    @SerializedName("confident")
    @Expose
    var confident: Boolean? = null
)