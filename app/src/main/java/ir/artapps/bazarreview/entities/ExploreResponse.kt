package ir.artapps.bazarreview.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ExploreResponse(
    @SerializedName("groups")
    @Expose
    var groups: List<Group>? = null,

    @SerializedName("totalResults")
    @Expose
    var totalResults: Int? = null
)