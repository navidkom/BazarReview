package ir.artapps.bazarreview.entities

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import ir.artapps.bazarreview.R
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Location(
    @SerializedName("address")
    @Expose
    var address: String? = null,

    @SerializedName("crossStreet")
    @Expose
    var crossStreet: String? = null,

    @SerializedName("lat")
    @Expose
    var lat: Double = 0.0,

    @SerializedName("lng")
    @Expose
    var lng: Double = 0.0,

    @SerializedName("labeledLatLngs")
    @Expose
    var labeledLatLngs: List<LabeledLatLng?>? = null,

    @SerializedName("distance")
    @Expose
    var distance: Int? = null,

    @SerializedName("postalCode")
    @Expose
    var postalCode: String? = null,

    @SerializedName("cc")
    @Expose
    var cc: String? = null,

    @SerializedName("city")
    @Expose
    var city: String? = null,

    @SerializedName("state")
    @Expose
    var state: String? = null,

    @SerializedName("country")
    @Expose
    var country: String? = null,

    @SerializedName("formattedAddress")
    @Expose
    var formattedAddress: List<String>? = null
) : Parcelable {

    override fun toString(): String {

        val ltrChar = "\u200e"
        val nlChar = "\n"
        val virChar = ", "

        val addressBuilder = StringBuilder()
        country?.let {
            addressBuilder.append(ltrChar, it)
        }
        city?.let {
            addressBuilder.append(ltrChar, virChar, it)
        }
        address?.let {
            addressBuilder.append(nlChar, ltrChar, it)
        }
        crossStreet?.let {
            addressBuilder.append(nlChar, ltrChar, it)
        }

        return addressBuilder.toString()
    }
}