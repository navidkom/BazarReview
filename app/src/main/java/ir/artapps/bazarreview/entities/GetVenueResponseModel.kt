package ir.artapps.bazarreview.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by navid on 27,December,2018
 */
data class GetVenueResponseModel(
    @SerializedName("meta")
    @Expose
    var meta: Meta,

    @SerializedName("response")
    @Expose
    var response: VenueResponse
)