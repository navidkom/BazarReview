package ir.artapps.bazarreview.entities

/**
 * Created by navid on 28,December,2018
 */
class VenuesPageEntity {
    var venues: List<Venue>
    var page: Int

    var isEndOfList = false

    constructor(venues: List<Venue>, page: Int) {
        this.venues = venues
        this.page = page
    }

    constructor(venues: List<Venue>, page: Int, endOfList: Boolean) {
        this.venues = venues
        this.page = page
        isEndOfList = endOfList
    }
}