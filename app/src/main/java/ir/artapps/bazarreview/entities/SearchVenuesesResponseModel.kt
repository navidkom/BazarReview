package ir.artapps.bazarreview.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * Created by navid on 20,December,2018
 */
data class SearchVenuesesResponseModel(
    @SerializedName("meta")
    @Expose
    var meta: Meta,

    @SerializedName("response")
    @Expose
    var response: ExploreResponse
)