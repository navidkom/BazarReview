package ir.artapps.bazarreview.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by navid on 22,December,2018
 */
data class VenueResponse(
    @SerializedName("venue")
    @Expose
    private var venues: Venue? = null,

    @SerializedName("confident")
    @Expose
    var confident: Boolean? = null
) {
    var venue: Venue?
        get() = venues
        set(venue) {
            venues = venues
        }
}