package ir.artapps.bazarreview.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("venue")
    @Expose
    var venue: Venue
)