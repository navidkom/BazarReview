package ir.artapps.bazarreview.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ExploreData(
    @SerializedName("meta")
    @Expose
    var meta: Meta? = null,
    @SerializedName("response")
    @Expose
    private var mExploreResponse: ExploreResponse? = null
) {

    fun getmExploreResponse(): ExploreResponse? {
        return mExploreResponse
    }

    fun setmExploreResponse(mExploreResponse: ExploreResponse?) {
        this.mExploreResponse = mExploreResponse
    }
}