package ir.artapps.bazarreview.ui.main.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ir.artapps.bazarreview.entities.Venue
import ir.artapps.bazarreview.ui.main.MainRecyclerViewAdapter

abstract class BaseViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    abstract fun onBind(venue: Venue, position: Int)
    abstract fun setClickListener(listener: MainRecyclerViewAdapter.OnItemClickListener)
}