package ir.artapps.bazarreview.ui.main.viewholder

import android.view.View
import ir.artapps.bazarreview.entities.Venue
import ir.artapps.bazarreview.ui.main.MainRecyclerViewAdapter

class LoadingViewHolder(v: View?) : BaseViewHolder(v!!) {
    override fun onBind(venue: Venue, position: Int) {}
    override fun setClickListener(listener: MainRecyclerViewAdapter.OnItemClickListener) {}
}