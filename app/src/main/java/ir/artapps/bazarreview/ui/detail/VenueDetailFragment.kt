package ir.artapps.bazarreview.ui.detail

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import ir.artapps.bazarreview.R
import ir.artapps.bazarreview.base.BaseDialogFragment
import ir.artapps.bazarreview.entities.Venue
import ir.artapps.bazarreview.ui.customview.VenueDetailCustomView
import kotlinx.android.synthetic.main.fragment_venue_detail.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by navid
 */
class VenueDetailFragment : BaseDialogFragment(), OnMapReadyCallback {
    private val MAP_ZOOM_LEVEL = 14.0f
    private var venue: Venue? = null
    private val venueDetailViewModel: VenueDetailViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_venue_detail, container, false)
        val mapFragment =
            activity?.supportFragmentManager?.findFragmentById(R.id.fragment_venue_detail_map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        return rootView
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.apply {
            if (containsKey(VENUE_MODEL_ARG_KEY)) {
                venue = getParcelable(VENUE_MODEL_ARG_KEY)
            }
        }

        toolbar?.apply {
            title = venue?.name
            context?.let {
                setTitleTextColor(ContextCompat.getColor(it, R.color.colorWhite))
            }
            setNavigationIcon(R.drawable.ic_nav_back)
            setNavigationOnClickListener { dismiss() }
        }

        collapsingToolbarLayout?.apply {
            context?.let {
                setCollapsedTitleTextColor(ContextCompat.getColor(it, R.color.colorWhite))
                setExpandedTitleColor(ContextCompat.getColor(it, R.color.colorWhite))
            }
        }

        appBarLayout?.setExpanded(false)

        setPassedData(venue)

        venueDetailViewModel.apply {
            venueLiveData.observe(
                viewLifecycleOwner,
                Observer { venue -> setNetData(venue) })
            venue?.id?.let {
                getVenueDetail(it)
            }
            errorLiveData.observe(viewLifecycleOwner, Observer {
                Snackbar.make(view, it, Snackbar.LENGTH_SHORT).show()
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.supportFragmentManager?.apply {
            findFragmentById(R.id.fragment_venue_detail_map_fragment)?.let {
                beginTransaction().remove(it).commitAllowingStateLoss()
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        venue?.apply {
            if (location?.lat != null && location?.lng != null) {
                val location = LatLng(location?.lat!!, location?.lng!!)

                googleMap.addMarker(
                    MarkerOptions().position(location)
                        .title(name)
                )
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, MAP_ZOOM_LEVEL))
                if (checkMapPermission()) {
                    googleMap.isMyLocationEnabled = true
                }
            }
        }
    }

    // set the venue data that passed from MainFragment
    private fun setPassedData(venue: Venue?) {
        venue?.apply {
            categories?.let {
                val categoryBuilder = StringBuilder()
                for ((_, name) in it) {
                    if (!name.isNullOrEmpty()) {
                        categoryBuilder.append("\u200e", name, "\n")
                    }
                }

                if (categoryBuilder.isNotEmpty()) {
                    categoryBuilder.deleteCharAt(categoryBuilder.length - 1)
                }
                addViewToMainView(categoryBuilder.toString(), R.drawable.ic_label_24)
            }

            location?.toString()?.let {
                if (it.isNotEmpty()) {
                    addViewToMainView(it, R.drawable.ic_location_24)
                }
            }
        }
    }

    private fun addViewToMainView(text: String?, drawableResource: Int) {
        context?.let {
            linearLayout?.addView(
                VenueDetailCustomView(
                    it,
                    text,
                    ContextCompat.getDrawable(it, drawableResource)
                )
            )
        }
    }

    // set the detail venue data that fetched from remote server
    private fun setNetData(venue: Venue?) {
        venue?.apply {
            bestPhoto?.let {
                Glide.with(this@VenueDetailFragment).load(it.imageUrl)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }

                        // listen when image is ready to expand appBarLayout ro better UX
                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            appBarLayout?.setExpanded(true)
                            return false
                        }

                    })
                    .into(photoImageView)
            }

            contact?.formattedPhone?.let {
                addViewToMainView(it, R.drawable.ic_phone_24)
            }

            rating?.let {
                addViewToMainView(it.toString(), R.drawable.ic_star_24)
            }

            shortUrl?.let {
                addViewToMainView(it, R.drawable.ic_web_24)
            }
        }
    }

    private fun checkMapPermission(): Boolean {
        context?.let {
            return !((ActivityCompat.checkSelfPermission(
                it,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
                    != PackageManager.PERMISSION_GRANTED) && ActivityCompat.checkSelfPermission(
                it,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED)
        }
        return false
    }

    companion object {
        private const val VENUE_MODEL_ARG_KEY = "venueModelKey"
        fun newInstance(venue: Venue?): VenueDetailFragment {
            val venueDetailFragment = VenueDetailFragment()
            val bundle = Bundle()
            bundle.putParcelable(VENUE_MODEL_ARG_KEY, venue)
            venueDetailFragment.arguments = bundle
            return venueDetailFragment
        }
    }
}