package ir.artapps.bazarreview.ui.main

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ir.artapps.bazarreview.entities.Venue
import ir.artapps.bazarreview.repo.VenueRepository
import ir.artapps.bazarreview.util.DistanceUtil
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class MainViewModel(private val venueRepository: VenueRepository) : ViewModel() {

    var latitude: Double = 0.0
    var longitude: Double = 0.0

    val venuesLiveData: LiveData<List<Venue>> = venueRepository.getVenuesLiveData()
    private val _errorLiveData: MutableLiveData<String> = MutableLiveData()
    val errorLiveData: LiveData<String> = _errorLiveData

    private val MIN_MOVEMENT_TO_RELOAD_DATA: Int = 100

    var loading = false
    var lastPage = false

    // reload data if location changes more than 'MIN_MOVEMENT_TO_RELOAD_DATA' meters
    fun updateLocation(location: Location) {

        if (DistanceUtil.distance(
                latitude,
                location.latitude,
                longitude,
                location.longitude
            ) > MIN_MOVEMENT_TO_RELOAD_DATA
        ) {
            latitude = location.latitude
            longitude = location.longitude
            getVenues(true)
        }

        latitude = location.latitude
        longitude = location.longitude
    }

    // this method requests venues from repository and handle returned error
    // result of venus will post on venuesLiveData and we dose not get result here
    fun getVenues(
        firstPage: Boolean
    ) {
        if (latitude == 0.0) {
            _errorLiveData.postValue(
                "AN ERROR OCCURRED: Location Is Not Available"
            )
            return
        }

        if (firstPage) lastPage = false
        viewModelScope.launch {
            val responseCode = venueRepository.getNearVenues(latitude, longitude, firstPage)

            // check if this is last page or not
            if (responseCode == 1) {
                lastPage = true
            }

            // error cases
            if (responseCode != 200 && responseCode != 1) {
                delay(3000)
                _errorLiveData.postValue(
                    when (responseCode) {
                        0 -> "AN ERROR OCCURRED: Network Error"
                        else -> "AN ERROR OCCURRED: Error Code $responseCode"
                    }
                )
            }
        }
    }
}

