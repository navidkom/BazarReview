package ir.artapps.bazarreview.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ir.artapps.bazarreview.entities.Venue
import ir.artapps.bazarreview.repo.VenueRepository
import kotlinx.coroutines.launch

/**
 * Created by navid
 */
class VenueDetailViewModel(private val venueRepository: VenueRepository) : ViewModel() {

    private val _venueLiveData = MutableLiveData<Venue>()
    val venueLiveData: LiveData<Venue> = _venueLiveData

    private val _errorLiveData: MutableLiveData<String> = MutableLiveData()
    val errorLiveData: LiveData<String> = _errorLiveData

    fun getVenueDetail(venueId: String) {
        viewModelScope.launch {
            val response = venueRepository.getVenue(venueId)
            if (response?.meta?.code == 200) {
                _venueLiveData.value = response.response.venue
            } else {
                _errorLiveData.value = "AN ERROR OCCURRED"
            }
        }
    }
}