package ir.artapps.bazarreview.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ir.artapps.bazarreview.R
import ir.artapps.bazarreview.ui.main.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow()
        }
    }
}
