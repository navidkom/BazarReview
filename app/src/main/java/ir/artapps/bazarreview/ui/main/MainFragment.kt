package ir.artapps.bazarreview.ui.main

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import ir.artapps.bazarreview.R
import ir.artapps.bazarreview.ui.detail.VenueDetailFragment
import ir.artapps.bazarreview.util.DistanceUtil
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainFragment : Fragment(), MainRecyclerViewAdapter.OnItemClickListener {

    private val linearLayoutManager = LinearLayoutManager(context)
    private val locationRequest = LocationRequest.create()
    private val MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1007

    private lateinit var locationCallback: LocationCallback
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var mAdapter: MainRecyclerViewAdapter

    private val venueViewModel: MainViewModel by viewModel()

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context?.let {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(it)
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                for (location in locationResult.locations) {
                    // Update UI with location data
                    venueViewModel.updateLocation(location)
                }
            }
        }
        checkLocationPermission()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = MainRecyclerViewAdapter()
        mAdapter.itemClickListener = this

        venueViewModel.apply {
            // observe updates of venues
            venuesLiveData.observe(viewLifecycleOwner,
                Observer {
                    it?.let {
                        // calculate distance from venue and user to show in view
                        if (latitude != 0.0 && longitude != 0.0) {
                            it.forEach { venue ->
                                venue.location?.let { loc ->
                                    venue.distance = DistanceUtil.distance(
                                        loc.lat,
                                        latitude,
                                        loc.lng,
                                        longitude
                                    )
                                }
                            }
                        }

                        mAdapter.setVenues(it)
                        refreshLayout.isRefreshing = false
                        mAdapter.loading = false
                        loading = false
                    }
                })

            // observe and show errors
            errorLiveData.observe(viewLifecycleOwner, Observer {
                loading = false
                mAdapter.loading = false
                refreshLayout.isRefreshing = false
                Snackbar.make(view, it, Snackbar.LENGTH_SHORT).show()
            })
        }

        recyclerView?.apply {
            layoutManager = linearLayoutManager
            adapter = mAdapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    linearLayoutManager.apply {

                        // check if recycler view moves to end and send request for next page
                        if (childCount + findFirstVisibleItemPosition() >= itemCount
                            && !venueViewModel.loading
                            && !venueViewModel.lastPage && dy > 0
                        ) {
                            venueViewModel.getVenues(false)
                            venueViewModel.loading = true
                            mAdapter.loading = true
                        }
                    }
                }
            })
        }

        // pull to refresh
        refreshLayout.setOnRefreshListener {
            venueViewModel.getVenues(true)
        }
    }

    private fun getLocationUpdates() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    mAdapter.loading = true
                    venueViewModel.updateLocation(it)
                }
            }

        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
    }

    private fun checkLocationPermission() {
        context?.let {
            if (ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // we can request the permission.
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
                )
            } else {
                getLocationUpdates()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    getLocationUpdates()
                }
            }
        }
    }

    // handle venue item clicks and show detail fragment
    override fun onItemClick(view: View?, position: Int) {
        val detailFragment: VenueDetailFragment =
            VenueDetailFragment.newInstance(mAdapter.items[position])
        detailFragment.show(childFragmentManager, position.toString())
    }
}
