package ir.artapps.bazarreview.remote

import ir.artapps.bazarreview.entities.GetVenueResponseModel
import ir.artapps.bazarreview.entities.SearchVenuesesResponseModel

/**
 * Created by navid
 */
interface VenueRemoteDataSource {

    suspend fun getVenues(
        latitude: Double,
        longitude: Double,
        offset: Int
    ): SearchVenuesesResponseModel

    suspend fun getVenueDetail(
        venueId: String?
    ): GetVenueResponseModel
}