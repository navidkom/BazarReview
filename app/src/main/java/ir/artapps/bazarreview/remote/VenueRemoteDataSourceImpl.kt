package ir.artapps.bazarreview.remote

/**
 * Created by navid
 */
class VenueRemoteDataSourceImpl: VenueRemoteDataSource {
    var CLIENT_ID = "LUZTWCG1I5SGFZ0DZ4ZLKYLEHG34P4Q0OLN5AP2M4SZASNKE"
    var CLIENT_SECRET = "HJX4AS4GRRRYJZ1UOPKZFW4NPHZE03XMHMGG2TIL4CKMGIQO"
    var VERSION = "20180101"
    var PAGE_SIZE = 20
    var RADIOS = 20000
    var SOURT_BY_DISTANCE = 1

    val service by lazy {
        ServiceGenerator.create()
    }

    override suspend fun getVenues(
        latitude: Double,
        longitude: Double,
        offset: Int
    ) = service.exploreVenuesAsync(
        String.format("%f,%f", latitude, longitude),
        CLIENT_ID,
        CLIENT_SECRET,
        VERSION,
        PAGE_SIZE,
        offset + 1,
        RADIOS,
        SOURT_BY_DISTANCE
    )

    override suspend fun getVenueDetail(
        venueId: String?
    ) = service.getVenueAsync(
        venueId,
        CLIENT_ID,
        CLIENT_SECRET,
        VERSION
    )
}