package ir.artapps.bazarreview.remote

import ir.artapps.bazarreview.entities.GetVenueResponseModel
import ir.artapps.bazarreview.entities.SearchVenuesesResponseModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by navid
 */
interface FoursquareApi {

    companion object {
        const val VEN_ID_KEY = "venues_Id"
        const val LATLANG_KEY = "ll"
        const val INTENT_KEY = "intent"
        const val CLIENT_ID_KEY = "client_id"
        const val SECRET_KEY = "client_secret"
        const val VERSION_KEY = "v"
        const val QUERY_LIMIT = "limit"
        const val QUERY_OFFSET = "offset"
        const val QUERY_RADIOS = "radius"
        const val QUERY_SORT = "sortByDistance"
    }

    @GET("venues/explore")
    suspend fun exploreVenuesAsync(
        @Query(LATLANG_KEY) latlang: String?,
        @Query(CLIENT_ID_KEY) clientId: String?,
        @Query(SECRET_KEY) secret: String?,
        @Query(VERSION_KEY) version: String?,
        @Query(QUERY_LIMIT) pageSize: Int,
        @Query(QUERY_OFFSET) offset: Int,
        @Query(QUERY_RADIOS) radios: Int,
        @Query(QUERY_SORT) sortFlag: Int?
    ): SearchVenuesesResponseModel

    @GET("venues/{$VEN_ID_KEY}")
    suspend fun getVenueAsync(
        @Path(VEN_ID_KEY) venueId: String?,
        @Query(CLIENT_ID_KEY) clientId: String?,
        @Query(SECRET_KEY) secret: String?,
        @Query(VERSION_KEY) version: String?
    ): GetVenueResponseModel
}